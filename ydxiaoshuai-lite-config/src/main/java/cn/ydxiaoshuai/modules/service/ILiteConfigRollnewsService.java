package cn.ydxiaoshuai.modules.service;

import cn.ydxiaoshuai.modules.entity.LiteConfigRollnews;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 首页滚动公告表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
public interface ILiteConfigRollnewsService extends IService<LiteConfigRollnews> {

}
