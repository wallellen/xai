package cn.ydxiaoshuai.modules.weixin.po;

import lombok.Data;

/**
 * @Description 授权成功返回的内容
 * @author 小帅丶
 * @className WXOAuth
 * @Date 2019/11/28-11:22
 **/
@Data
public class WXOAuth {
    /** 错误码 */
    private String errcode;
    /** 错误信息 */
    private String errmsg;
    /** 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同 */
    private String access_token;
    /** access_token接口调用凭证超时时间，单位（秒） */
    private String expires_in;
    /** 用户刷新access_token */
    private String refresh_token;
    /** 用户唯一标识，请注意，在未关注公众号时，用户访问公众号的网页，也会产生一个用户和公众号唯一的OpenID */
    private String openid;
    /** 用户授权的作用域，使用逗号（,）分隔 */
    private String scope;
}
