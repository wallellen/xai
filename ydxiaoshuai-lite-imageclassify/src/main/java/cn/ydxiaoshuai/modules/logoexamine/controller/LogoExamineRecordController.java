package cn.ydxiaoshuai.modules.logoexamine.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.ydxiaoshuai.common.api.vo.Result;
import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.constant.StatusConts;
import cn.ydxiaoshuai.common.factory.BDFactory;
import cn.ydxiaoshuai.common.sdkpro.AipImageClassifyPro;
import cn.ydxiaoshuai.common.system.base.controller.JeecgController;
import cn.ydxiaoshuai.common.system.query.QueryGenerator;
import cn.ydxiaoshuai.modules.logoexamine.entity.LogoExamineRecord;
import cn.ydxiaoshuai.modules.logoexamine.service.ILogoExamineRecordService;
import com.baidu.aip.imageclassify.AipImageClassify;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: LOGO自定义上传审核记录表
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Slf4j
@Api(tags="LOGO自定义上传审核记录表")
@RestController
@RequestMapping("/logoexamine/logoExamineRecord")
public class LogoExamineRecordController extends JeecgController<LogoExamineRecord, ILogoExamineRecordService> {
	@Autowired
	private ILogoExamineRecordService logoExamineRecordService;
	//图像识别
	AipImageClassifyPro aipImageClassify = BDFactory.getAipImageClassifyPro();
	
	/**
	 * 分页列表查询
	 *
	 * @param logoExamineRecord
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "LOGO自定义上传审核记录表-分页列表查询")
	@ApiOperation(value="LOGO自定义上传审核记录表-分页列表查询", notes="LOGO自定义上传审核记录表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(LogoExamineRecord logoExamineRecord,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<LogoExamineRecord> queryWrapper = QueryGenerator.initQueryWrapper(logoExamineRecord, req.getParameterMap());
		Page<LogoExamineRecord> page = new Page<LogoExamineRecord>(pageNo, pageSize);
		IPage<LogoExamineRecord> pageList = logoExamineRecordService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param logoExamineRecord
	 * @return
	 */
	@AutoLog(value = "LOGO自定义上传审核记录表-添加")
	@ApiOperation(value="LOGO自定义上传审核记录表-添加", notes="LOGO自定义上传审核记录表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody LogoExamineRecord logoExamineRecord) {
		logoExamineRecordService.save(logoExamineRecord);
		return Result.ok("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param logoExamineRecord
	 * @return
	 */
	@AutoLog(value = "LOGO自定义上传审核记录表-编辑")
	@ApiOperation(value="LOGO自定义上传审核记录表-编辑", notes="LOGO自定义上传审核记录表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody LogoExamineRecord logoExamineRecord) {
		//0待审核 1审核通过 2审核失败
		if(StatusConts.LOGO_EXAMINE_APPROVED.equals(logoExamineRecord.getExaminStatus())){

		}
		logoExamineRecordService.updateById(logoExamineRecord);
		return Result.ok("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "LOGO自定义上传审核记录表-通过id删除")
	@ApiOperation(value="LOGO自定义上传审核记录表-通过id删除", notes="LOGO自定义上传审核记录表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		logoExamineRecordService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "LOGO自定义上传审核记录表-批量删除")
	@ApiOperation(value="LOGO自定义上传审核记录表-批量删除", notes="LOGO自定义上传审核记录表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.logoExamineRecordService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "LOGO自定义上传审核记录表-通过id查询")
	@ApiOperation(value="LOGO自定义上传审核记录表-通过id查询", notes="LOGO自定义上传审核记录表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		LogoExamineRecord logoExamineRecord = logoExamineRecordService.getById(id);
		return Result.ok(logoExamineRecord);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param logoExamineRecord
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, LogoExamineRecord logoExamineRecord) {
      return super.exportXls(request, logoExamineRecord, LogoExamineRecord.class, "LOGO自定义上传审核记录表");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, LogoExamineRecord.class);
  }

}
