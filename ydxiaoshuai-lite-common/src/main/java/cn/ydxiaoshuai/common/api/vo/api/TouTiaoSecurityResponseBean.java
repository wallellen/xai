package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className TouTiaoSecurityResponseBean
 * @Description 安全监测响应对象
 * @Date 2020/6/17-16:55
 **/

@NoArgsConstructor
@Data
public class TouTiaoSecurityResponseBean {

    private String log_id;
    private List<DataBean> data;
    private String error_id;
    private long code;
    private String message;
    private String exception;

    @NoArgsConstructor
    @Data
    public static class DataBean {

        private String msg;
        private int code;
        private String task_id;
        private Object data_id;
        private List<PredictsBean> predicts;

        @NoArgsConstructor
        @Data
        public static class PredictsBean {

            private int prob;
            private boolean hit;
            private String target;
            private String model_name;
        }
    }
}
