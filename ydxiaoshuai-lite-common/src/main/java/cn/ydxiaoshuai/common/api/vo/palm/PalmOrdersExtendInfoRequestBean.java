package cn.ydxiaoshuai.common.api.vo.palm;

import lombok.Data;

/**
 * @Description 扩展信息
 * @author 小帅丶
 * @className PalmOrdersExtendInfoRequestBean
 * @Date 2020/1/3-11:11
 **/
@Data
public class PalmOrdersExtendInfoRequestBean {

    /**
     * palm_id : 7d3c4b748cbf42a3b4a5bf0ecd941565202001
     * name :
     * is_share : 0
     * index_version : 1
     */

    private String palm_id;
    private String name;
    private int is_share=0;
    private int index_version=1;
}
