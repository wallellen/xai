package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;

/**
 * @author 小帅丶
 * @className BaiDuBase
 * @Description 百度接口基础类
 * @Date 2020/9/24-15:28
 **/
@Data
public class BaiDuBase {
    private Integer error_code;
    private String error_msg;
    private long log_id;
}
