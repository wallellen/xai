package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className GarbageBean
 * @Description 穿衣搭配返回页面的对象
 * @Date 2020年4月26日
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RecommendBean extends BaseBean {
    /** 具体返回的内容 */
    private RecommendResponseBean.ResultBean data;
    /** 几套衣服 */
    private Integer num;
    public RecommendBean success(String msg, String msg_zh, Integer num, RecommendResponseBean.ResultBean data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.num = num;
        this.data = data;
        return this;
    }
    public RecommendBean fail(String msg, String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public RecommendBean error(String msg, String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}
