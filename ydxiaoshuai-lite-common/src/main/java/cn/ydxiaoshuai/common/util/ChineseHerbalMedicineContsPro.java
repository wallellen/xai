package cn.ydxiaoshuai.common.util;


import cn.ydxiaoshuai.common.api.vo.api.EasyDLChinesResponseBean;

/**
 * @Description 中草药
 * @author 小帅丶
 * @className ChineseHerbalMedicineConts
 * @Date 2020/3/12-10:26
 **/
public enum ChineseHerbalMedicineContsPro {
    HUANGQIN("huangqin","黄芩","腐肠,黄文,空肠,元芩,土金茶","苦","归肺,胆,脾,大肠,小肠经","清热药","清热燥湿,泻火解毒,止血,安胎","根"),
    CHAIHU("chaihu","柴胡","柴草,北柴胡,红柴胡,硬柴胡,软柴胡","苦","归肝,胆经","解表药","和表解里,疏肝,升阳","根"),
    LIANQIAO("lianqiao","连翘","旱连子,大翘子,空壳","苦","归肺,心,小肠经","清热药","清热解毒,消肿散结","果实"),
    JINGJIE("jingjie","荆芥","香荆芥","辛","归肺,肝经","解表药","解表散风,透疹","地上部分"),
    SHIGAO("shigao","石膏","细理石,软石膏,白虎","辛,甘","归肺,胃经","清热药","清热泻火,除烦止渴","硫酸盐类矿物"),
    FANGFENG("fangfeng","防风","屏风,关防风,东防风","辛,甘","归膀胱,肝,脾经","解表药","解表祛风,胜湿,止痉","根"),
    QIANGHUO("qianghuo","羌活","川羌,蚕羌,竹节羌,大头羌","辛,苦","归膀胱,肾经","解表药","散寒,祛风,除湿,止痛","根茎及根"),
    XIXIN("xixin","细辛","小辛,细草,独叶草,金盆草,山人参","辛","归肺,肾,心经","解表药","祛风散寒,通窍止痛,温肺化饮","全草"),
    XINYI("xinyi","辛夷","迎春,木笔花,毛辛夷,姜朴花","辛","归肺,胃经","解表药","散风寒,鼻通窍","花蕾"),
    CHANTUI("chantui","蝉蜕","蝉壳,枯蝉,蝉衣,知了皮","甘","归肺,肝经","解表药","散风除热,利咽,透疹,退翳,止痉","羽化后的蜕壳"),
    SHENGMA("shengma","升麻","周升麻,鬼脸升麻,绿升麻,北升麻,川升麻","辛,微甘","归肺,胃,脾,大肠经","解表药","发表透疹,清热解毒,升举阳气","根茎"),
    GEGEN("gegen","葛根","干葛,甘葛,粉葛","甘,辛","归脾,胃经","解表药","解肌退热,生津,透疹,升阳止泻","根"),
    ZHIMU("zhimu","知母","连母,水须,穿地龙","苦,甘","归肺,胃,肾经","清热药","清热泻火,生津润燥","根茎"),
    XIAKUCAO("xiakucao","夏枯草","铁线夏枯,大头花,白花草,六月干,棒槌草","苦,辛","归肝,胆经","清热药","清火,明目,散结,消肿","果穗"),
    HUANGBAI("huangbai","黄柏","檗木,檗皮,黄檗","苦","归肾,膀胱经","清热药","清热燥湿,泻火除蒸,解毒疗疮","除去栓皮的树皮"),
    KUSHEN("kushen","苦参","苦骨,川参,凤凰爪,牛参","苦","归心,肝,胃,大肠,膀胱经","清热药","清热燥湿,杀虫,利尿","根"),
    DAQINGYE("daqingye","大青叶","大青","苦","归心,胃经","清热药","清热解毒,凉血消斑","叶片"),
    CHUANXINLIAN("chuanxinlian","穿心莲","一见喜,苦胆草,四方莲,金香草,苦草,印度草","苦","归心,肺,大肠,膀胱经","清热药","清热解毒,凉血,消肿","全草"),
    PUGONGYING("pugongying","蒲公英","蒲公草,地丁,婆婆丁,黄花草,蒲公丁","甘,苦","归肝,胃经","清热药","清热解毒,消肿散结,利尿通淋","带根全草"),
    ZIHUADIDING("zihuadiding","紫花地丁","紫地丁,兔耳草,辽堇菜","苦,辛","归心,肝经","清热药","清热解毒,凉血消肿","带根全草"),
    YEJUHUA("yejuhua","野菊花","野菊","辛,苦","归肺,肝经","清热药","清热解毒","头状花序"),
    SHEGAN("shegan","射干","剪刀草,黄远,扁竹,乌扇","苦","归肺经","清热药","清热解毒,消痰,利咽","根茎"),
    BAITOUWENG("baitouweng","白头翁","野丈人,胡王使者,白头公","苦","归胃,大肠经","清热药","清热解毒,凉血止痢","根"),
    BAIHUASHESHECAO("baihuasheshecao","白花蛇舌草","蛇舌草,目目生珠草,羊须草,蛇总管","微苦,甘","归胃,大肠,肝经","清热药","清热解毒,利湿通淋","全草"),
    MUDANPI("mudanpi","牡丹皮","牡丹根皮,丹皮,丹根","苦,辛","归心,肝,肾经","清热药","清热凉血,活血散瘀","根皮"),
    CHISHAO("chishao","赤芍","木芍药,红芍药,赤芍药,臭牡丹根","苦","归肝经","清热药","清热凉血,散瘀止痛","根"),
    BAISHAO("baishao","白芍","金芍药,白芍药","苦,酸,甘","归肝,脾经","补虚药","平肝止痛,养血调经,敛阴止汗","根"),
    ZICAO("zicao","紫草","紫草茸,山紫草,紫丹,紫草根,红石根","甘,咸","归心,肝经","清热药","凉血,活血,解毒透疹","根"),
    SHUINIUJIAO("shuiniujiao","水牛角","沙牛角","苦","归心,肝经","平肝息风药","清热解毒,凉血,定惊","角"),
    QINGHAO("qinghao","青蒿","蒿,草蒿,黑蒿,野兰蒿","苦,辛","归肝,胆经","清热药","清热解暑,除蒸,截疟","全草"),
    DIGUPI("digupi","地骨皮","杞根,地骨,枸杞根,枸杞根皮,红榴根皮","甘","归肺,肝,肾经","清热药","凉血除蒸,清肺降火","根皮"),
    PEILAN("peilan","佩兰","兰,兰草,大泽兰,香水兰,女兰,香草,千金草","辛","归脾,胃,肺经","化湿药","芳香化湿,醒脾开胃,发表解暑","地上部分"),
    CANGZHU("cangzhu","苍术","赤术,青术,仙术","苦,辛","归脾,胃经","化湿药","燥湿健脾,祛风散寒,明目","根茎"),
    SHAREN("sharen","砂仁","缩砂仁,缩砂蜜","辛","归脾,胃经","化湿药","化湿开胃,温脾止泻,理气安胎","成熟果实"),
    CAOGUO("caoguo","草果","草果子,草果仁","辛","归脾,胃经","化湿药","燥湿温中,除痰截疟","成熟果实"),
    FULING("fuling","茯苓","茯灵,云苓,松薯,松苓","甘,淡","归心,肺,脾,肾经","利水渗湿药","利水渗湿,健脾宁心","菌核"),
    YIYIREN("yiyiren","薏苡仁","薏米,起实,薏珠子,回回米,米仁,苡仁,苡米,六谷子","甘,淡","归脾,胃,肺经","利水渗湿药","健脾渗湿,除痹止泻,清热排脓","成熟种仁"),
    TONGCAO("tongcao","通草","白通草,通花,大通草,大木通,五角加皮","甘,淡","归肺,胃经","利水渗湿药","清热利尿,通气下乳","茎髓"),
    JINQIANCAO("jinqiancao","金钱草","地钱,连钱草,铜钱草,金钱薄荷,一串钱,大叶金钱草,破铜钱","甘,咸","归肝,胆,肾,膀胱经","利水渗湿药","清利湿热,通淋,消肿","全草"),
    HUZHANG("huzhang","虎杖","大虫杖,苦杖,酸杖,斑杖,苦杖根,杜牛膝,酸桶笋,土地榆,酸通,雄黄连,活血龙","苦","归肝,胆,肺经","利水渗湿药","祛风利湿,散瘀定痛,止咳化痰","根茎和根"),
    FUZI("fuzi","附子","侧子,虎掌,熟白附子,黑附子,明附片,刁附,川附子","辛,甘","归心,肾,脾经","温里药","回阳救逆,补火助阳,逐风寒湿邪用于亡阳虚脱","子根的加工品"),
    GANJIANG("ganjiang","干姜","白姜,均姜,干生姜","辛","归脾,胃,肾,心,肺经","温里药","温中散寒,回阳通脉,燥湿消痰","干燥根茎"),
    ROUGUI("rougui","肉桂","牡桂,紫桂,大桂,辣桂,桂皮,玉桂","辛,甘","归脾,肾,心,肝经","温里药","补火助阳,引火归源,散寒止痛,活血通经","树皮"),
    XIAOHUIXIANG("xiaohuixiang","小茴香","茴香,土茴香,野茴香,谷茴香,谷香,香子","辛","归肝,脾,胃,肾经","温里药","散寒止痛,理气和中","成熟果实"),
    CHENPI("chenpi","陈皮","橘皮,贵老,黄橘皮,红皮","辛,苦","归脾,肺经","理气药","理气健脾,燥湿化痰","成熟果皮"),
    MUXIANG("muxiang","木香","蜜香,青木香,五香,五木香,南木香,广木香","辛,苦","归肺,胃,大肠,胆,三焦经","理气药","行气止痛,健脾消食","根"),
    CHENXIANG("chenxiang","沉香","蜜香,沉水香","辛,苦","归脾,胃,肾经","理气药","行气止痛,温中止呕,纳气平喘","含有树脂的木材"),
    XIANGFU("xiangfu","香附","雀头香,莎草根,香附子,雷公头,香附米,三棱草根","辛,微苦,微甘","归肝,脾,三焦经","理气药","行气解郁,调经止痛","根茎"),
    FOSHOU("foshou","佛手","佛手柑,佛手香橼,蜜罗柑,福寿柑,五指柑","辛,苦,酸","归肝,脾,肺经","理气药","舒肝理气,和胃止痛","果实"),
    SHANZHA("shanzha","山楂","鼠查子,山里红果,山里果子,映山红果,海红,酸查","酸,甘","归脾,胃,肝经","消食药","消食健胃,行气散瘀","成熟果实"),
    SHENQU("shenqu","神曲","六神曲","甘,辛","归脾,胃经","消食药","消食和胃","成熟果实经发芽干燥而成"),
    MAIYA("maiya","麦芽","麦蘖,大麦毛,大麦芽","甘","归脾,胃,肝经","消食药","行气消食,健脾开胃,退乳消胀","成熟果实经发芽干燥而成"),
    LAIFUZI("laifuzi","莱菔子","萝卜子","辛,甘","归脾,胃,肺经","消食药","消食除胀,降气化痰","种子"),
    JINEIJIN("jineijin","鸡内金","鸡肫内黄皮,鸡肫皮,鸡黄皮,鸡食皮,鸡合子,鸡中金,化石胆,化骨胆","甘","归脾,胃,小肠,膀胱经","消食药","消食健胃,涩精止遗","砂囊内壁"),
    HUAIHUA("huaihua","槐花","槐蕊","苦","归肝,大肠经","止血药","凉血止血,清肝泻火","花蕾"),
    CEBAIYE("cebaiye","侧柏叶","柏叶,丛柏叶","苦,涩","归肺,肝,大肠经","止血药","凉血止血,生发乌发","嫩枝叶"),
    BAIMAOGEN("baimaogen","白茅根","茅根,白花茅根,坚草根,甜草根,寒草根","甘","归肺,胃,膀胱经","止血药","凉血止血,清热利尿","根茎"),
    SANQI("sanqi","三七","山漆,金不换,血参,人参三七,田三七","甘,微苦","归肝,胃经","止血药","散瘀止血,消肿定痛","根"),
    YUJIN("yujin","郁金","黄郁","辛,苦","归肝,胆,心经","活血化瘀药","行气化瘀,清心解郁,利胆退黄","块根"),
    JIANGHUANG("jianghuang","姜黄","宝鼎香,黄姜","辛,苦","归肝,脾经","活血化瘀药","活血行气,通经止痛","根茎"),
    DANSHEN("danshen","丹参","赤参,紫丹参,红根,活血根,红参,血参根","苦","归心,肝经","活血化瘀药","祛瘀止痛,活血通经,清心除烦","根及根茎"),
    HONGHUA("honghua","红花","红蓝花,刺红花,草红花","辛","归心,肝经","活血化瘀药","活血通经,散瘀止痛","花"),
    TAOREN("taoren","桃仁","桃核仁","苦,甘","归心,肝,大肠经","活血化瘀药","活血祛瘀,润肠通便","成熟种子"),
    YIMUCAO("yimucao","益母草","益母,茺蔚,坤草,益母蒿,月母草,红花益母草","苦,辛","归肝,心,膀胱经","活血化瘀药","活血调经,利尿消肿","地上部分"),
    ZELAN("zelan","泽兰","虎兰,小泽兰,地瓜儿苗,奶孩,风药,蛇王菊,草泽兰","苦,辛","归肝,脾经","活血化瘀药","活血化瘀,行水消肿","地上部分"),
    JIXUETENG("jixueteng","鸡血藤","血风藤","苦,甘","归肝经","活血化瘀药","补血,活血,通络","藤茎"),
    CHUANSHANJIA("chuanshanjia","穿山甲","川山甲,山甲,甲片,鳞片,鲮鲤甲","咸","归肝,胃经","活血化瘀药","通经下乳,消肿排脓,搜风通络","鳞片"),
    BANXIA("banxia","半夏","地文,水玉,羊眼半夏,地珠半夏,珠半夏,天落星,无心菜根,地巴豆","辛","归脾,胃,肺经","化痰止咳平喘药","燥湿化痰,降逆止呕,消痞散结","根茎"),
    TIANNANXING("tiannanxing","天南星","虎掌,半夏精,南星,虎掌南星,三棒子,野芋头,斑杖","苦,辛","归肺,肝,脾经","化痰止咳平喘药","燥湿化痰,祛风止痉,散结消肿","块茎"),
    ZHEBEIMU("zhebeimu","浙贝母","土贝母,象贝,浙贝,象贝母,大贝母","苦","归肺,心经","化痰止咳平喘药","清热散结,化痰止咳","鳞茎"),
    ZHURU("zhuru","竹茹","竹皮,青竹茹,淡竹茹,麻巴,竹二青","甘","归肺,胃经","化痰止咳平喘药","清热化痰,除烦止呕","茎的中间层"),
    CISHI("cishi","磁石","玄石,磁君,吸铁石,铁石,处石","咸","归心,肝,肾经","安神药","平肝潜阳,聪耳明目,镇惊安神,纳气平喘","矿石"),
    LONGGU("longgu","龙骨","陆虎遗生,那伽骨","甘,涩","归心,肝,肾经","安神药","镇惊安神,平肝潜阳,收敛固涩","骨骼的化石"),
    SUANZAOREN("suanzaoren","酸枣仁","枣仁,酸枣核","甘,酸","归心,肝,胆经","安神药","补肝,宁心,敛汗,生津","成熟种子"),
    BAIZIREN("baiziren","柏子仁","柏实,柏子,柏仁,侧柏子","甘","归心,肾,大肠经","安神药","养心安神,止汗,润肠","种仁"),
    HESHOUWU("heshouwu","何首乌","地精,首乌,陈知白,红内消,马肝石,黄花乌根,小独根","苦,甘,涩","归肝,心,肾经","补虚药","解毒,消痈,润肠通便","块根"),
    HEHUANPI("hehuanpi","合欢皮","合昏皮,夜合皮,合欢木皮","甘","归心,肝经","安神药","解郁安神,活血消肿","树皮"),
    YUANZHI("yuanzhi","远志","葽绕,棘葾,苦远志","苦,辛","归心,肾,肺经","安神药","安神益智,祛痰,消肿","根"),
    ZHENZHUMU("zhenzhumu","珍珠母","珠牡,珠母,明珠母","咸","归肝,心经","平肝息风药","平肝潜阳,定惊明目","蚌壳或珍珠贝"),
    MULI("muli","牡蛎","蛎蛤,左顾牡蛎,牡蛤,海蛎子壳,海蛎子皮,左壳","咸","归肝,胆,肾经","平肝息风药","平肝潜阳,软坚散结,收敛固涩","贝壳"),
    GOUTENG("gouteng","钩藤","钓藤,吊藤,钓钩藤,钓钩勾,莺爪风,嫩钩钩,双钩藤,鹰爪风,倒钩刺","甘","归肝,心包经","平肝息风药","清热平肝,息风定痉","带钩茎枝"),
    DILONG("dilong","地龙","广地龙","咸","归肝,脾,膀胱经","平肝息风药","清热定惊,通络,平喘,利尿","全虫体"),
    QUANXIE("quanxie","全蝎","全虫,主薄虫,杜伯,茯背虫","辛","归肝经","平肝息风药","息风镇痉,攻毒散结,通络止痛","干燥体"),
    JIANGCAN("jiangcan","僵蚕","白僵蚕,天虫,僵虫","咸","归肝,肺经","平肝息风药","祛风定惊,化痰散结","发病致死干燥体"),
    RENSHEN("renshen","人参","土精,神草,黄参,血参,地精,金井玉阑,孩儿参,棒棰","甘,微苦","归心,肺,脾经","补虚药","大补元气,复脉固脱,补脾益肺,生津,安神","根"),
    BAIZHU("baizhu","白术","术,山芥,山姜,山连,山精,冬白术","苦,甘","归脾,胃经","补虚药","补气健脾,燥湿利水,止汗,安胎","根茎"),
    SHANYAO("shanyao","山药","薯蓣,山芋,淮山药,野山豆,山板术,九黄姜,白药子,野白薯,佛掌薯","甘","归脾,肺,肾经","补虚药","益气养阴,补脾肺肾,固精止带","根茎"),
    BAIBIANDOU("baibiandou","白扁豆","扁豆","甘","归脾,胃经","补虚药","健脾化湿,和中消暑","成熟种子"),
    DUZHONG("duzhong","杜仲","思仙,木绵,思仲,丝连皮,扯丝皮,丝绵皮","甘","归肝,肾经","补虚药","补肝肾,强筋骨,安胎","树皮"),
    TUSIZI("tusizi","菟丝子","菟丝实,吐丝子,黄藤子,龙须子,萝丝子,缠龙子,黄萝子,豆须子","甘","归肝,肾,脾经","补虚药","滋补肝肾,安胎,明目,止泻","成熟种子"),
    TIANDONG("tiandong","天冬","天门冬,大当门根","甘,苦","归肺,肾经","补虚药","养阴润燥,清肺生津","块根"),
    SHIHU("shihu","石斛","林兰,禁生,杜兰,金钗花,千年润,黄草,吊兰花","甘","归胃,肾经","补虚药","益胃生津,滋阴清热","茎"),
    HUANGJING("huangjing","黄精","龙衔,太阳草,白及,野生姜,山姜,玉竹黄精,鸡头参","甘","归脾,肺,肾经","补虚药","补气养阴,健脾,润肺,益肾","根茎"),
    GOUQIZI("gouqizi","枸杞子","甜菜子,杞子,枸杞果,枸杞豆,地骨子","甘","归肝,肾经","补虚药","滋补肝肾,益精明目","成熟果实"),
    MOHANLIAN("mohanlian","墨旱莲","旱莲草","甘,酸","归肝,肾经","补虚药","滋补肝肾,凉血止血","地上部分"),
    NVZHENZI("nvzhenzi","女贞子","女贞实,冬青子,白蜡树子,鼠梓子","甘,苦","归肝,肾经","补虚药","滋补肝肾,明目乌发","成熟果实"),
    BIEJIA("biejia","鳖甲","上甲,鳖壳,团甲鱼,鳖盖子","咸","归肝,肾经","补虚药","滋阴潜阳,软坚散结,退热除蒸","背甲"),
    WUMEI("wumei","乌梅","梅实,熏梅,桔梅肉","酸,涩","归肝,脾,肺,大肠经","收涩药","敛肺,涩肠,安蛔,生津","近成熟果实"),
    WUWEIZI("wuweizi","五味子","玄及,会及,五梅子","酸,甘","归肺,心,肾经","收涩药","收敛固涩,益气生津,补肾宁心","成熟果实"),
    ROUDOUKOU("roudoukou","肉豆蔻","豆蔻,肉果","辛","归脾,胃,大肠经","收涩药","温中行气,涩肠止泻","成熟种仁"),
    CHISHIZHI("chishizhi","赤石脂","赤符,红高岭,赤石土,吃油脂,红土","甘,涩","归大肠,胃经","收涩药","涩肠,止血,生肌敛疮","矿物"),
    SHANZHUYU("shanzhuyu","山茱萸","薯枣,鸡足,山萸肉,实枣,肉枣,枣皮,萸肉,药枣","酸,涩","归肝,肾经","收涩药","补益肝肾,涩精固脱","成熟果肉"),
    SANGPIAOXIAO("sangpiaoxiao","桑螵蛸","桑蛸,螵蛸,螳螂子,螳螂蛋,螳螂壳","甘,咸","归肝,肾经","收涩药","益肾固精,缩尿,止浊","卵鞘"),
    LIANZIXIN("lianzixin","莲子心","薏,苦薏,莲薏,莲心","苦","归心,肾经","收涩药","清心安神,交通心肾,涩精止血","幼叶及胚根"),
    QIANHU("qianhu","前胡","姨妈菜,罗鬼菜,土当归,水前胡","苦,辛","归肺经","化痰止咳平喘药","散风清热,降气化痰","根"),
    ZHISHI("zhishi","枳实","洞庭,黏刺","苦,辛,酸","归脾,胃经","理气药","破气消积,化痰散痞","幼果"),
    TUBIECHONG("tubiechong","土鳖虫","地鳖,土鳖,过街,地乌龟,节节虫,臭虫母","咸","归肝经","活血化瘀药","破瘀血,续筋骨","雌体的全体"),
    DAXUETENG("daxueteng","大血藤","血藤,红皮藤,红藤,红血藤,红菊花心,山红藤,花血藤","苦","归大肠,肝经","清热药","清热解毒,活血,祛风","藤茎"),
    NIUXI("niuxi","牛膝","百倍,怀牛膝,鸡胶骨","苦,甘,酸","归肝,肾经","活血化瘀药","补肝肾,强筋骨,逐瘀通经,引血下行","根"),
    DAFUPI("dafupi","大腹皮","槟榔皮,槟榔壳,大腹毛,茯毛,槟榔衣,大腹绒","辛","归脾,胃,大肠,小肠经","利水渗湿药","下气宽中,行水消肿","果皮"),
    SHICHANGPU("shichangpu","石菖蒲","昌本,菖蒲,昌阳,苦菖蒲,山菖蒲,石蜈蚣,水蜈蚣,香草,剑叶菖蒲","辛,苦","归心,胃经","开窍药","化湿开胃,开窍豁痰,醒神益智","根茎"),
    BAIBU("baibu","百部","嗽药,百条根,野天门冬,百奶,九十九条根,山百根,牛虱鬼","甘,苦","归肺经","化痰止咳平喘药","润肺下气止咳,杀虫","块茎"),
    XIANHECAO("xianhecao","仙鹤草","龙牙草,老鹳嘴,子母草,草龙牙,父子草,狼牙草","苦,涩","归肺,肝,脾经","止血药","收敛止血,截疟,止痢,解毒","全草"),
    XUDUAN("xuduan","续断","龙豆,接骨,南草,接骨草,川断","苦,甘,辛","归肝,肾经","补虚药","补肝肾,强筋骨,续折伤,止崩漏","根"),
    PAOJIANG("paojiang","炮姜","黑姜","辛","归脾,肝经","止血药","温中散寒,温经止血","根茎"),
    DIYU("diyu","地榆","白地榆,鼠尾地榆,涩地榆,赤地榆,红地榆,山枣参,水槟榔","苦,酸,涩","归肝,大肠经","止血药","凉血止血,解毒敛疮","根"),
    WULINGZHI("wulingzhi","五灵脂","药本,寒号虫粪,寒雀粪","苦,咸,甘","归肝经","活血化瘀药","活血止痛,化瘀止血","粪便"),
    PUHUANG("puhuang","蒲黄","蒲厘花粉,蒲花,蒲草黄,蒲棒花粉","甘","归肝,心经","止血药","止血,化瘀,通淋","花粉"),
    GUYA("guya","谷芽","蘖米,谷蘖,稻蘖,稻芽","甘","归脾,胃经","消食药","消食和中,健脾开胃","成熟果实经发芽干燥而成"),
    JINYINHUA("jinyinhua","金银花","忍冬花,银花,双花,二花,金藤花","甘","归肺,心,胃经","清热药","清热解毒,凉散风热","花蕾"),
    SHECHUANGZI("shechuangzi","蛇床子","蛇米,蛇珠,蛇床仁,蛇床实,气果,双肾子","辛,苦","归肾经","外用药","温肾壮阳,燥湿,祛风,杀虫","成熟果实"),
    WUJIAPI("wujiapi","五加皮","南五加皮","辛,苦","归肝,肾经","祛风湿药","祛风湿,补肝肾,强筋骨","根皮"),
    LULUTONG("lulutong","路路通","枫实,枫香果,枫球子,枫树球,枫木","辛,苦","归肝,肾经","祛风湿药","祛风活络,利水,通经","成熟果实"),
    BAJITIAN("bajitian","巴戟天","巴戟,鸡肠风,兔子肠","甘,辛","归肾,肝经","补虚药","补肾阳,强筋骨,祛风湿","根"),
    TIANKUIZI("tiankuizi","天葵子","紫背天葵子,千年老鼠屎","甘,苦","归肝经","清热药","清热解毒,消肿散结","块根"),
    BAIFAN("baifan","白矾","矾石,理石,白君,明矾,生矾,云母矾","酸,涩","归肺,脾,肝,大肠经","外用药","外用解毒杀虫,燥湿止痒；内服止血止泻,祛风除痰","明矾石经加工提炼制成结晶"),
    GUANZHONG("guanzhong","贯众","贯节,贯渠,百头,虎卷,黑狗脊,贯仲,管仲","苦","归肝,脾经","清热药","清热解毒,驱虫","根茎及叶柄残基"),
    SANGSHEN("sangshen","桑椹","桑实,乌椹,文武实,黑椹,桑枣,桑椹子","甘,酸","归心,肝,肾经","补虚药","补血滋阴,生津润燥","果穗"),
    LIZHIHE("lizhihe","荔枝核","荔仁,枝核,大荔核","辛,微苦","归肝,肾经","理气药","行气散结,散寒止痛","成熟种子"),
    LUOSHITENG("luoshiteng","络石藤","络石,云花,石龙藤,络石草","苦","归心,肝经","祛风湿药","祛风通络,凉血消肿","带叶茎藤"),
    SHUIHONGHUAZI("shuihonghuazi","水红花子","水荭子,荭草实","咸","归肝,胃经","活血化瘀药","散血消癥,消积止痛","成熟果实"),
    HUOMAREN("huomaren","火麻仁","麻子,麻子仁,大麻子,大麻仁,火麻子","甘","归肺,胃,大肠经","泻下药","润肠通便","成熟种子"),
    GANCAO("gancao","甘草","美草,密甘,密草,国老,粉草,甜草,甜根子,棒草","甘","归心,肺,脾,胃经","补虚药","补脾益气,清热解毒,祛痰止咳,缓急止痛,调和诸药","根及根茎"),
    BAIHE("baihe","百合","白百合","甘","归心,肺经","补虚药","","肉质鳞茎");
    private String medicine_pinyin;//拼音
    private String medicine_name;//中文名称
    private String medicine_alias;//别名
    private String medicine_taste;//药味
    private String medicine_meridian_tropism;//归经
    private String medicine_category;//功能类型
    private String medicine_function;//功能
    private String medicinal_parts;//药用部位

    public String getMedicine_pinyin() {
        return medicine_pinyin;
    }

    public void setMedicine_pinyin(String medicine_pinyin) {
        this.medicine_pinyin = medicine_pinyin;
    }

    public String getMedicine_name() {
        return medicine_name;
    }

    public void setMedicine_name(String medicine_name) {
        this.medicine_name = medicine_name;
    }

    public String getMedicine_alias() {
        return medicine_alias;
    }

    public void setMedicine_alias(String medicine_alias) {
        this.medicine_alias = medicine_alias;
    }

    public String getMedicine_taste() {
        return medicine_taste;
    }

    public void setMedicine_taste(String medicine_taste) {
        this.medicine_taste = medicine_taste;
    }

    public String getMedicine_meridian_tropism() {
        return medicine_meridian_tropism;
    }

    public void setMedicine_meridian_tropism(String medicine_meridian_tropism) {
        this.medicine_meridian_tropism = medicine_meridian_tropism;
    }

    public String getMedicine_category() {
        return medicine_category;
    }

    public void setMedicine_category(String medicine_category) {
        this.medicine_category = medicine_category;
    }

    public String getMedicine_function() {
        return medicine_function;
    }

    public void setMedicine_function(String medicine_function) {
        this.medicine_function = medicine_function;
    }

    public String getMedicinal_parts() {
        return medicinal_parts;
    }

    public void setMedicinal_parts(String medicinal_parts) {
        this.medicinal_parts = medicinal_parts;
    }

    ChineseHerbalMedicineContsPro(String medicine_pinyin, String medicine_name, String medicine_alias, String medicine_taste, String medicine_meridian_tropism, String medicine_category, String medicine_function, String medicinal_parts) {
        this.medicine_pinyin = medicine_pinyin;
        this.medicine_name = medicine_name;
        this.medicine_alias = medicine_alias;
        this.medicine_taste = medicine_taste;
        this.medicine_meridian_tropism = medicine_meridian_tropism;
        this.medicine_category = medicine_category;
        this.medicine_function = medicine_function;
        this.medicinal_parts = medicinal_parts;
    }

    /**
     * @Author 小帅丶
     * @Description 根据药品拼音查询药品的基本属性
     * @Date  2020/3/12
     * @param medicine_pinyin 药品拼音
     * @return java.lang.String
     **/
    public static EasyDLChinesResponseBean getChineseHerbalMedicineByPinyin(String medicine_pinyin){
        for (ChineseHerbalMedicineContsPro herbalMedicineConts : ChineseHerbalMedicineContsPro.values()) {
            if(medicine_pinyin.equals(herbalMedicineConts.getMedicine_pinyin())){
                EasyDLChinesResponseBean chinesResponseBean = new EasyDLChinesResponseBean();
                chinesResponseBean.setMedicine_pinyin(herbalMedicineConts.getMedicine_pinyin());
                chinesResponseBean.setMedicine_name(herbalMedicineConts.getMedicine_name());
                chinesResponseBean.setMedicine_alias(herbalMedicineConts.getMedicine_alias());
                chinesResponseBean.setMedicine_taste(herbalMedicineConts.getMedicine_taste());
                chinesResponseBean.setMedicine_meridian_tropism(herbalMedicineConts.getMedicine_meridian_tropism());
                chinesResponseBean.setMedicine_category(herbalMedicineConts.getMedicine_category());
                chinesResponseBean.setMedicine_function(herbalMedicineConts.getMedicine_function());
                chinesResponseBean.setMedicinal_parts(herbalMedicineConts.getMedicinal_parts());
                return chinesResponseBean;
            }
        }
        return null;
    }
}
