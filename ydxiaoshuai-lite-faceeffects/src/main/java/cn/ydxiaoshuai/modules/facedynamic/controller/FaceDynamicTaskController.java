package cn.ydxiaoshuai.modules.facedynamic.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.ydxiaoshuai.common.api.vo.Result;
import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.system.base.controller.JeecgController;
import cn.ydxiaoshuai.common.system.query.QueryGenerator;
import cn.ydxiaoshuai.modules.facedynamic.entity.FaceDynamicTask;
import cn.ydxiaoshuai.modules.facedynamic.service.IFaceDynamicTaskService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 人脸动态任务
 * @Author: 小帅丶
 * @Date:   2021-05-13
 * @Version: V1.0
 */
@Slf4j
@Api(tags="人脸动态任务")
@RestController
@RequestMapping("/facedynamic/faceDynamicTask")
public class FaceDynamicTaskController extends JeecgController<FaceDynamicTask, IFaceDynamicTaskService> {
	@Autowired
	private IFaceDynamicTaskService faceDynamicTaskService;

	/**
	 * 分页列表查询
	 *
	 * @param faceDynamicTask
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "人脸动态任务-分页列表查询")
	@ApiOperation(value="人脸动态任务-分页列表查询", notes="人脸动态任务-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FaceDynamicTask faceDynamicTask,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FaceDynamicTask> queryWrapper = QueryGenerator.initQueryWrapper(faceDynamicTask, req.getParameterMap());
		Page<FaceDynamicTask> page = new Page<FaceDynamicTask>(pageNo, pageSize);
		IPage<FaceDynamicTask> pageList = faceDynamicTaskService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 * 添加
	 *
	 * @param faceDynamicTask
	 * @return
	 */
	@AutoLog(value = "人脸动态任务-添加")
	@ApiOperation(value="人脸动态任务-添加", notes="人脸动态任务-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FaceDynamicTask faceDynamicTask) {
		faceDynamicTaskService.save(faceDynamicTask);
		return Result.ok("添加成功！");
	}

	/**
	 * 编辑
	 *
	 * @param faceDynamicTask
	 * @return
	 */
	@AutoLog(value = "人脸动态任务-编辑")
	@ApiOperation(value="人脸动态任务-编辑", notes="人脸动态任务-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FaceDynamicTask faceDynamicTask) {
		faceDynamicTaskService.updateById(faceDynamicTask);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "人脸动态任务-通过id删除")
	@ApiOperation(value="人脸动态任务-通过id删除", notes="人脸动态任务-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		faceDynamicTaskService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "人脸动态任务-批量删除")
	@ApiOperation(value="人脸动态任务-批量删除", notes="人脸动态任务-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.faceDynamicTaskService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "人脸动态任务-通过id查询")
	@ApiOperation(value="人脸动态任务-通过id查询", notes="人脸动态任务-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FaceDynamicTask faceDynamicTask = faceDynamicTaskService.getById(id);
		return Result.ok(faceDynamicTask);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param faceDynamicTask
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, FaceDynamicTask faceDynamicTask) {
      return super.exportXls(request, faceDynamicTask, FaceDynamicTask.class, "人脸动态任务");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, FaceDynamicTask.class);
  }

}
