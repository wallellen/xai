package cn.ydxiaoshuai.modules.util;

import cn.hutool.http.HttpRequest;
import cn.ydxiaoshuai.common.api.vo.api.TouTiaoSecurityRequestBean;
import cn.ydxiaoshuai.common.api.vo.api.TouTiaoSecurityResponseBean;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * @author 小帅丶
 * @className TouTiaoSecurityUtil
 * @Description 头条接口工具类
 * @Date 2020/6/17-16:54
 **/
@Component
@Slf4j
public class TouTiaoSecurityUtil {
    private static String PICTURE_DETECT_URL = "https://developer.toutiao.com/api/v2/tags/image/";
    /**
     * @Author 小帅丶
     * @Description 图片内容安全检测
     * @Date  2020/6/17 17:02
     * @param bean 请求的参数
     * @param accessToken 请求的token
     * @return cn.ydxiaoshuai.tools.vo.TouTiaoSecurityResponseBean
     **/
    public TouTiaoSecurityResponseBean pictureDetect(TouTiaoSecurityRequestBean bean, String accessToken){
        String result = HttpRequest.post(PICTURE_DETECT_URL).header("X-Token", accessToken).body(JSON.toJSONString(bean)).execute().body();
        log.info("头条获取AccessToken返回的内容:"+result);
        TouTiaoSecurityResponseBean responseBean = JSON.parseObject(result, TouTiaoSecurityResponseBean.class);
        return responseBean;
    }
}
